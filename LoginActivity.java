package curso.umg.gt.practicaumg1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText nom, ape, pass, confpassw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nom=(EditText)findViewById(R.id.nombre);
        ape = (EditText) findViewById(R.id.apellido);
        pass = (EditText) findViewById(R.id.password);
        confpassw = (EditText) findViewById(R.id.confpass);

    }

    public void login(View view) {
        Intent i = null;


        String nombre=nom.getText().toString();
        String apellido=ape.getText().toString();
        String password=pass.getText().toString();
        String confpass=confpassw.getText().toString();




        if (password.equals(confpass)){
            Intent uno =new Intent(this, PreferenciaActivity.class);
            startActivity(uno);

           Toast notificacion= Toast.makeText(this,"Bienvenido",Toast.LENGTH_SHORT);
            notificacion.show();
            nom.setText("");
            ape.setText("");
            pass.setText("");
            confpassw.setText("");
        }else {
            Toast notificacion= Toast.makeText(this,"Credenciales invalidas",Toast.LENGTH_SHORT);
            notificacion.show();
            pass.setText("");
            confpassw.setText("");
        }

    }

    public void Cancelar(View view) {
        nom.setText("");
        ape.setText("");
        pass.setText("");
        confpassw.setText("");
    }

    public void onClick(View view) {
        nom.setText("");
        ape.setText("");
        pass.setText("");
        confpassw.setText("");
    }
}
